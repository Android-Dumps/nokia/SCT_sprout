#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_SCT_sprout.mk

COMMON_LUNCH_CHOICES := \
    lineage_SCT_sprout-user \
    lineage_SCT_sprout-userdebug \
    lineage_SCT_sprout-eng
